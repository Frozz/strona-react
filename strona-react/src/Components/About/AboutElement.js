import React from 'react';

function AboutElement(props) {

  return (
    <div className="about-dog">
    <div className={`about-dog-img about-dog-img--${props.img}`}></div>
    <div className="about-dog-text">
      <h2>{props.title}</h2>
      <p>
        Lorem ipsum dolor sit amet consectetur
        adipisicing elit.
        Assumenda dolorem vel eum pariatur earum quae
        soluta cumque quia deserunt!
        neque corporis voluptatum amet mollitia enim ab,
        obcaecati eius nam!
        Lorem ipsum dolor sit amet consectetur
        adipisicing elit. Harum officia, corrupti,
        labore
        modi non deleniti alias saepe adipisci excepturi
        aliquid consequuntur atque perspiciatis hic
        re
        pellat aut porro quas ea illo!
            </p>
    </div>
  </div>
  )
}

export default AboutElement;