import React from 'react';
import './Components Styles/LandingPage.css';

function LandingPage() {

  return (
    <section className="landing-page">
      <div className="landing-page-shadow">
        <div className="container">
          <div className="landing-page-text">
            <h1>Hodowla psów rasy Akita Inu</h1>
            <h2>Zarezerwuj swojego szczeniaka jeszcze dziś !</h2>
            <button onClick={window.scrollTo(500,0)}className="landing-page-btn">oferta</button>
          </div>
        </div>
      </div>
    </section>
  )
}

export default LandingPage;