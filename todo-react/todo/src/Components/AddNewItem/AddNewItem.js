import React from 'react';
import './AddNewItem.css';

const AddNewItem = ({updateNewTodoName,inputValue,stateValue,addTodo}) => {
  return (
    <div className="addItemWrapper">
      <label htmlFor="todoInput">Create new TODO:</label>
      <input id="todoInput" className="addItemInput" onChange={updateNewTodoName} value={inputValue}></input>
      <button onClick={addTodo} className="addItemBtn">Add</button>
    </div>
  )
}

export default AddNewItem;

  